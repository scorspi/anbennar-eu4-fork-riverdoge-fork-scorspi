
setup_vision = yes
government = native
add_government_reform = leechfather_cult_reform
government_rank = 1
primary_culture = leechmen
religion = leechfather
technology_group = tech_leechmen
capital = 2222

historical_rival = G15
historical_rival = G16
historical_rival = G17
historical_rival = G18
historical_rival = G20

1443.1.10 = {
	monarch = {
		name = "Ixaxahuanirti"            # Heavy flow of blood or a bloody rain
		dynasty = "Golihuani"         # Lineage
		adm = 5                # ADM skill of the ruler
		dip = 4                # DIP skill of the ruler
		mil = 6               # MIL skill of the ruler
		birth_date = 1419.1.1         # Used to set the age of the ruler. Derived from the date of the history entry the monarch scope is found in.
		female = no                # Whether the ruler is female
		regent = no                # Whether the ruler is considered a regency
		religion = leechfather       # The religion of the ruler
		culture = leechmen       # The culture of the ruler
		
		# Makes the defined ruler a leader
		leader = {
			name = "Ixaxahuanirti Golihuani"     # Name of the leader
			type = general       # Type of leader: general, admiral, explorer, conquistidor
			fire = 2        # Fire value of the leader
			shock = 8       # Shock value of the leader
			manuever = 5    # Manuever value of the leader
			siege = 3      # Siege value of the leader
		}
	}
	add_ruler_personality = mage_personality
	set_ruler_flag = conjuration_1
	set_ruler_flag = enchantment_1
	set_ruler_flag = enchantment_2
}