estate_lowercastes =
{
	icon = 9

	# If true, country will get estate
	trigger = {
		NOT = { government = native }
		NOT = { has_reform = cossacks_reform }
		NOT = { has_reform = siberian_tribe }
		is_nomad = no

		#Anbennar
		#Raheni cultures have castes, if they follow High Philosophy
		religion = high_philosophy

		# Must have certain cultures or follow a particular religious school:
		OR = {
			primary_culture = royal_harimari
			culture_group = upper_raheni
			culture_group = middle_raheni
			culture_group = south_raheni
			religious_school = {
				group = raheni
				school = golden_palace_school
			}
		}
		
		#Rajnadhaga ends castes through missions
		NOT = { has_country_flag = rajnadhaga_end_caste_system}
	}

	# These scale with loyalty & power
	country_modifier_happy = {
		global_tax_modifier = 0.2
		papal_influence = 1
		devotion = 0.5
		church_power_modifier = 0.1
		monthly_fervor_increase = 1
		stability_cost_modifier = -0.1
	}
	country_modifier_neutral = {
		global_tax_modifier = 0.2
	}
	country_modifier_angry = {
		global_tax_modifier = -0.1
		papal_influence = -1
		devotion = -0.5
		church_power_modifier = -0.25
		monthly_fervor_increase = -1
		stability_cost_modifier = 0.1
		global_unrest = 2
	}

	land_ownership_modifier = {
		lowercastes_loyalty_modifier = 0.2
	}

	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 0.1
			NOT = { religion_group = owner }
		}
		modifier = {
			factor = 0.5
			NOT = { religion = owner }
		}
		modifier = {
			factor = 0.67
			NOT = { is_state_core = owner }
		}
		modifier = {
			factor = 1.2
			base_tax = 10
		}
	}

	# Influence modifiers
	base_influence = 10
	influence_modifier = {
		desc = EST_VAMPIRIC_LORD
		trigger = {
			has_estate_privilege = estate_vampires_organization_vampires_lord
		}
		influence = -10
	}
	influence_modifier = { #TODO: change this to something more logical?
		desc = EST_VAL_STATE_RELIGIOUS_MINORITY
		trigger = {
			NOT = { dominant_religion = ROOT }
		}
		influence = -15
	}
	influence_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER
		trigger = {
			OR = {
				has_disaster = estate_nobility_disaster
				has_disaster = estate_burghers_disaster
				has_disaster = estate_mages_disaster
				has_disaster = estate_artificers_disaster
				has_disaster = estate_adventurers_disaster
				#has_disaster = estate_uppercastes_disaster #TODO: create disaster
				#has_disaster = estate_middlecastes_distaster #TODO: create disaster
				#has_disaster = estate_brahmins_disaster
				#has_disaster = estate_jains_disaster
			}
		}
		influence = -40
	}

	loyalty_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER_LOY
		trigger = {
			OR = {
				has_disaster = estate_nobility_disaster
				has_disaster = estate_burghers_disaster
				has_disaster = estate_mages_disaster
				has_disaster = estate_artificers_disaster
				has_disaster = estate_adventurers_disaster
				#has_disaster = estate_uppercastes_disaster #TODO: create disaster
				#has_disaster = estate_middlecastes_distaster #TODO: create disaster
				#has_disaster = estate_brahmins_disaster
				#has_disaster = estate_jains_disaster
			}
		}
		loyalty = -20
	}

	color = { 200 150 0 }

	privileges = {
		estate_lowercastes_organization_caste
		estate_lowercastes_land_rights
		estate_lowercastes_unions
		estate_lowercastes_independent_experts
		estate_lowercastes_new_world_colonies
		estate_lowercastes_supporting_congregations
		estate_lowercastes_less_oversight
		estate_church_for_the_faith
		estate_lowercastes_monopoly_of_incense
		estate_lowercastes_monopoly_of_wool
		estate_lowercastes_monopoly_of_wine
		estate_lowercastes_monopoly_of_slaves
		estate_church_guaranteed_autonomy
	}

	agendas = {
		estate_lowercastes_develop_x
		estate_lowercastes_build_temple_in_y
		estate_lowercasters_build_manufactory_in_y
		estate_lowercastes_convert_province_x
		estate_lowercastes_reduce_war_exhaustion
		estate_lowercastes_recover_stability
		estate_lowercastes_increase_stability
		estate_lowercastes_reduce_overextension
		estate_lowercastes_restore_devotion
		estate_lowercastes_root_out_heresy
		estate_lowercastes_crush_religious_revolts
		estate_lowercastes_protect_brethren
		estate_lowercastes_hire_advisor
		estate_lowercastes_fire_advisor
	}
	influence_from_dev_modifier = 1.0
}
