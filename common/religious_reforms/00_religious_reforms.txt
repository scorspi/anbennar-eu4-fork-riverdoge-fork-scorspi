# Do not change tags in here without changing every other reference to them.
# Do not change tags in here without changing every other reference to them.
# If adding new groups or ideas, make sure they are unique.

# ai_will do does not actually determine weighting, but AI will not pick an idea with ai_will_do = 0
# ROOT = country picking the idea
# groups set to colonial=yes will be higher prioritized by the AI when spending power

leechfather_reforms = {
    trigger = {
        religion = leechfather
    }    

    can_buy_idea = {
		NOT {
			doom = 50
		}
		custom_trigger_tooltip = {
			tooltip = tt_leechdens_subjects
			AND = {
				G15 = {
					OR = {
						is_subject_of = ROOT
						tag = ROOT
						exists = no
					}
				}
				G16 = {
					OR = {
						is_subject_of = ROOT
						tag = ROOT
						exists = no
					}
				}
				G17 = {
					OR = {
						is_subject_of = ROOT
						tag = ROOT
						exists = no
					}
				}
				G18 = {
					OR = {
						is_subject_of = ROOT
						tag = ROOT
						exists = no
					}
				}
				G19 = {
					OR = {
						is_subject_of = ROOT
						tag = ROOT
						exists = no
					}
				}
				G20 = {
					OR = {
						is_subject_of = ROOT
						tag = ROOT
						exists = no
					}
				}
			}
		}
    }
    
    leechfather_reform_1 = {
        global_monthly_devastation = -0.15
    }
    leechfather_reform_2 = {
        free_mil_policy = 1
    }
    leechfather_reform_3 = {
        global_sailors_modifier = 0.15
    }
    leechfather_reform_4 = {
        culture_conversion_cost = -0.25
    }
    leechfather_reform_5 = {
        colonists = 1
    }     
    
    ai_will_do = {
        factor = 1
    }
}
