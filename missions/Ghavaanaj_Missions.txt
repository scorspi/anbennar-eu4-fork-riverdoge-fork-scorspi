
ghavaanaj_1_1 = {
	slot = 1
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			culture = ghavaanaj
		}
	}
	
	ghavaanaj_1_gather_mahouts = {
		icon = mission_noble_council
		position = 1
		
		required_missions = {
			
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			has_leader_with = {
				general = yes
				shock = 4
			}
		}
		
		effect = {
			#! Reward
		}
	}
	
	ghavaanaj_1_organise_elephant_training = {
		icon = mission_noble_council
		position = 2
		
		required_missions = {
			ghavaanaj_1_gather_mahouts
			ghavaanaj_2_maintain_reeshi_relations
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			num_of_cavalry = 4
			mil_power = 100
		}
		
		effect = {
			add_mil_power = -100
			if = {
				limit = { ai = yes }
				#! Reward for ai
			}
			else = {
				custom_tooltip = ghavaanaj_elephant_herd_mechanic_unlocked_tt
				hidden_effect = {
					set_country_flag = ghavaanaj_elephant_herd_mechanic
					set_variable = {
						which = ghavaanaj_max_herd_size
						value = 4
					}
					set_variable = {
						which = ghavaanaj_herd_size_counter
						value = 0
					}
					set_variable = {
						which = ghavaanaj_herd_training_counter
						value = 0
					}
					add_country_modifier = {
						name = ghavaanaj_herd_training_mental_00
						duration = -1
					}
					add_country_modifier = {
						name = ghavaanaj_herd_training_physical_00
						duration = -1
					}
					add_country_modifier = {
						name = ghavaanaj_herd_size_1
						duration = -1
					}
					save_global_event_target_as = ghavaanaj_herd_country #! This will only work for one country, if there are more countries with this mechanic it breaks down, future fix?
					random_owned_province = { add_province_triggered_modifier = ghavaanaj_monthly_tick }
					ghavaanaj_update_herd_size_provinces = yes
				}
			}
		}
	}
	
	ghavaanaj_1_reeshi_bonds = {
		icon = mission_noble_council
		position = 3
		
		required_missions = {
			ghavaanaj_1_organise_elephant_training
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			estate_influence = {
				estate = estate_mages
				influence = 60
			}
			estate_loyalty = {
				estate = estate_mages
				loyalty = 60
			}
			dip_power = 100
			mil_power = 100
		}
		
		effect = {
			add_dip_power = -100
			add_mil_power = -100
			set_estate_privilege = estate_mages_reeshi_bonds
			hidden_effect = { set_country_flag = ghavaanaj_reeshi_bonds_flag } #! Give modifiers/effects
		}
	}
}


ghavaanaj_1_2 = {
	slot = 1
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			tag = R72
		}
	}
	
	ghavaanaj_1_merge_breeding_stock = {
		icon = mission_noble_council
		position = 4
		
		required_missions = {
			ghavaanaj_1_reeshi_bonds
			ghavaanaj_2_formalize_tribal_relations
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4469
				province_id = 4479
				province_id = 4508
				province_id = 4486
				province_id = 4477
				province_id = 4483
			}
			NOT = {
				owned_by = ROOT
			}
		}
		
		trigger = {
			owns_core_province = 4469
			owns_core_province = 4479
			owns_core_province = 4508
			owns_core_province = 4486
			owns_core_province = 4477
			owns_core_province = 4483
		}
		
		effect = {
			change_variable = {
				which = ghavaanaj_max_herd_size
				value = 6
			}
		}
	}
	
	ghavaanaj_1_refined_training = {
		icon = mission_noble_council
		position = 5
		
		required_missions = {
			ghavaanaj_1_merge_breeding_stock
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4469
				province_id = 4479
				province_id = 4508
				province_id = 4486
				province_id = 4477
				province_id = 4483
			}
			NOT = {
				has_province_modifier = ghavaanaj_white_herd_unlocked
				has_province_modifier = ghavaanaj_red_herd_unlocked
				has_province_modifier = ghavaanaj_purple_herd_unlocked
				has_province_modifier = ghavaanaj_black_herd_unlocked
				has_province_modifier = ghavaanaj_palace_herd_unlocked
				has_province_modifier = ghavaanaj_castle_herd_unlocked
			}
		}
		
		trigger = {
			4469 = { has_province_modifier = ghavaanaj_white_herd_unlocked }
			4479 = { has_province_modifier = ghavaanaj_red_herd_unlocked }
			4508 = { has_province_modifier = ghavaanaj_purple_herd_unlocked }
			4486 = { has_province_modifier = ghavaanaj_black_herd_unlocked }
			4477 = { has_province_modifier = ghavaanaj_palace_herd_unlocked }
			4483 = { has_province_modifier = ghavaanaj_castle_herd_unlocked }
		}
		
		effect = {
			custom_tooltip = ghavaanaj_1_refined_training_tt
			hidden_effect = {
				set_country_flag = ghavaanaj_refined_training_flag
			}
		}
	}
	
	ghavaanaj_1_expand_the_herd = {
		icon = mission_noble_council
		position = 6
		
		required_missions = {
			ghavaanaj_1_refined_training
			ghavaanaj_2_war_halls
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			ghavaanaj_has_herd_size_6 = yes
		}
		
		effect = {
			add_country_modifier = {
				name = ghavaanaj_widely_available_workforce
				duration = 7300
			}
		}
	}
	
	ghavaanaj_1_pachyderm_expertise = {
		icon = mission_noble_council
		position = 8
		
		required_missions = {
			ghavaanaj_1_expand_the_herd
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			has_global_modifier_value = {
				which = cavalry_power
				value = 0.5
			}
		}
		
		effect = {
			add_government_reform = pachyderm_masters
			custom_tooltip = ghavaanaj_1_pachyderm_expertise_tt
		}
	}
	
	ghavaanaj_1_champion_elephantine = {
		icon = mission_noble_council
		position = 9
		
		required_missions = {
			ghavaanaj_1_pachyderm_expertise
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			has_global_modifier_value = {
				which = cavalry_power
				value = 0.5
			}
			mil = 6
			capital_scope = {
				has_ruler_leader_from = ROOT
				cavalry_in_province = 20
			}
		}
		
		effect = {
			add_ruler_modifier = {
				name = ghavaanaj_champion_elephantine
				duration = -1
			}
		}
	}
	
	ghavaanaj_1_pachyderm_mortars = {
		icon = mission_noble_council
		position = 10
		
		required_missions = {
			ghavaanaj_2_lords_of_tusk_and_trunk
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			mil_power = 100
			num_of_owned_provinces_with = {
				value = 3
				trade_goods = iron
			}
		}
		
		effect = {
			add_mil_power = -100
			add_country_modifier = {
				name = ghavaanaj_pachyderm_mortars
				duration = 9125
			}
		}
	}
}


ghavaanaj_2_1 = {
	slot = 2
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			culture = ghavaanaj
		}
	}
	
	ghavaanaj_2_maintain_reeshi_relations = {
		icon = mission_noble_council
		position = 1
		
		required_missions = {
			
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			estate_loyalty = {
				estate = estate_mages
				loyalty = 60
			}
			estate_influence = {
				estate = estate_mages
				influence = 40
			}
		}
		
		effect = {
			#! Reward
		}
	}
	
	ghavaanaj_2_the_desert_tribes = {
		icon = mission_noble_council
		position = 2
		
		required_missions = {
			ghavaanaj_3_prepare_the_elephants
		}
		
		provinces_to_highlight = {
			OR = {
				area = maldeelakhan_area
				area = banjisbid_area
				province_id = 4461
				province_id = 4472
			}
			NOT = {
				AND = {
					owned_by = ROOT
					culture = ghavaanaj
					NOT = { nationalism = 1 }
				}
			}
		}
		
		trigger = {
			maldeelakhan_area = {
				type = all
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
			banjisbid_area = {
				type = all
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
			4461 = {
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
			4472 = {
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
		}
		
		effect = {
			add_army_tradition = 10
		}
	}
	
	ghavaanaj_2_formalize_tribal_relations = {
		icon = mission_noble_council
		position = 3
		
		required_missions = {
			ghavaanaj_2_the_desert_tribes
			ghavaanaj_3_the_small_tribes
			ghavaanaj_4_the_black_tribe
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			dip_power = 100
		}
		
		effect = {
			add_dip_power = -100
			add_government_reform = ghavaanaj_tribes
		}
	}
	
	ghavaanaj_2_harness_the_alliance = {
		icon = mission_noble_council
		position = 4
		
		required_missions = {
			ghavaanaj_2_formalize_tribal_relations
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			tribal_allegiance = 40
		}
		
		effect = {
			add_manpower = 10
			capital_scope = {
				cavalry = ROOT
				cavalry = ROOT
				cavalry = ROOT
				cavalry = ROOT
			}
		}
	}
}


ghavaanaj_2_2 = {
	slot = 2
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			tag = R72
		}
	}
	
	ghavaanaj_2_war_halls = {
		icon = mission_noble_council
		position = 5
		
		required_missions = {
			ghavaanaj_2_harness_the_alliance
		}
		
		provinces_to_highlight = {
			owned_by = ROOT
			culture = ghavaanaj
			NOT = {
				has_manpower_building_trigger = yes
				has_building = farm_estate
			}
		}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 5
				culture = ghavaanaj
				has_manpower_building_trigger = yes
				has_building = farm_estate
			}
		}
		
		effect = {
			custom_tooltip = ghavaanaj_2_war_halls_tt
			hidden_effect = {
				every_owned_province = {
					limit = { culture = ghavaanaj has_manpower_building_trigger = yes has_building = farm_estate }
					add_base_manpower = 2
					add_permanent_province_modifier = {
						name = ghavaanaj_war_hall
						duration = -1
					}
				}
			}
		}
	}
	
	ghavaanaj_2_expand_grazing_grounds = {
		icon = mission_noble_council
		position = 6
		
		required_missions = {
			ghavaanaj_3_a_new_black_king
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_2_trunks_in_the_dhenbasana = {
		icon = mission_noble_council
		position = 7
		
		required_missions = {
			ghavaanaj_2_expand_grazing_grounds
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_2_feeding_the_herd = {
		icon = mission_noble_council
		position = 8
		
		required_missions = {
			ghavaanaj_2_trunks_in_the_dhenbasana
			ghavaanaj_3_the_white_guild
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 20
				OR = {
					trade_goods = grain
					trade_goods = livestock
				}
				has_building = farm_estate
			}
		}
		
		effect = {
			change_variable = {
				which = ghavaanaj_max_herd_size
				value = 10
			}
		}
	}
	
	ghavaanaj_2_lords_of_tusk_and_trunk = {
		icon = mission_noble_council
		position = 9
		
		required_missions = {
			ghavaanaj_2_feeding_the_herd
			ghavaanaj_1_pachyderm_expertise
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			ghavaanaj_has_herd_size_18 = yes
		}
		
		effect = {
			add_country_modifier = {
				name = ghavaanaj_lords_of_tusk_and_trunk
				duration = -1
			}
		}
	}
	
	ghavaanaj_2_the_white_bull = {
		icon = mission_noble_council
		position = 10
		
		required_missions = {
			ghavaanaj_2_lords_of_tusk_and_trunk
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			has_global_modifier_value = {
				which = cavalry_power
				value = 1
			}
		}
		
		effect = {
			add_country_modifier = {
				name = ghavaanaj_the_white_bull
				duration = -1
			}
		}
	}
	
	ghavaanaj_2_the_great_trumpet = {
		icon = mission_noble_council
		position = 11
		
		required_missions = {
			ghavaanaj_2_the_white_bull
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			capital_scope = {
				cavalry_in_province = 50
			}
		}
		
		effect = {
			#! Reward the Great Trumpet Event
		}
	}
}


ghavaanaj_3_1 = {
	slot = 3
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			culture = ghavaanaj
		}
	}
	
	ghavaanaj_3_prepare_the_elephants = {
		icon = mission_noble_council
		position = 1
		
		required_missions = {
			
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			army_size_percentage = 0.8
		}
		
		effect = {
			banjiraal_area = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
			buhaanjirgal_area = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
			banjisbid_area = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
			maldeelakhan_area = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
			4482 = {
				if = {
					limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
					add_permanent_claim = ROOT
				}
			}
			4481 = {
				if = {
					limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
					add_permanent_claim = ROOT
				}
			}
			4472 = {
				if = {
					limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
					add_permanent_claim = ROOT
				}
			}
			4461 = {
				if = {
					limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
					add_permanent_claim = ROOT
				}
			}
		}
	}
	
	ghavaanaj_3_the_small_tribes = {
		icon = mission_noble_council
		position = 2
		
		required_missions = {
			ghavaanaj_3_prepare_the_elephants
		}
		
		provinces_to_highlight = {
			OR = {
				area = banjiraal_area
			}
			NOT = {
				AND = {
					owned_by = ROOT
					culture = ghavaanaj
					NOT = { nationalism = 1 }
				}
			}
		}
		
		trigger = {
			banjiraal_area = {
				type = all
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
		}
		
		effect = {
			add_army_tradition = 10
		}
	}
	
	ghavaanaj_3_the_black_kings_city = {
		icon = mission_noble_council
		position = 3
		
		required_missions = {
			ghavaanaj_2_the_desert_tribes
			ghavaanaj_3_the_small_tribes
			ghavaanaj_4_the_black_tribe
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4485
			}
			NOT = {
				owned_by = ROOT
			}
		}
		
		trigger = {
			owns_core_province = 4485
		}
		
		effect = {
			add_legitimacy = 25
			4485 = { move_capital_effect = yes }
		}
	}
	
	ghavaanaj_3_a_new_black_king = {
		icon = mission_noble_council
		position = 5
		
		required_missions = {
			ghavaanaj_3_the_black_kings_city
			ghavaanaj_2_harness_the_alliance
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			estate_loyalty = {
				estate = estate_middlecastes
				loyalty = 60
			}
			estate_loyalty = {
				estate = estate_uppercastes
				loyalty = 60
			}
			tribal_allegiance = 60
			legitimacy = 100
		}
		
		effect = {
			add_power_projection = {
				type = mission_rewards_power_projection
				amount = 25
			}
			add_country_modifier = {
				name = ghavaanaj_a_new_black_king
				duration = 9125
			}
			dhujat_region = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
			satarsaya_area = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
			tiltaghar_area = {
				limit = { NOT = { is_core = ROOT is_permanent_claim = ROOT } }
				add_permanent_claim = ROOT
			}
		}
	}
}


ghavaanaj_3_2 = {
	slot = 3
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			tag = R72
		}
	}
	
	ghavaanaj_3_dhujati_tusks = {
		icon = mission_noble_council
		position = 6
		
		required_missions = {
			ghavaanaj_3_a_new_black_king
		}
		
		provinces_to_highlight = {
			OR = {
				area = khirnadhiman_area
				area = keyattordha_area
				area = khiraspid_area
				area = tughayasa_area
			}
			NOT = {
				owned_by = ROOT
			}
		}
		
		trigger = {
			owns_core_province = 4497
			num_of_owned_provinces_with = {
				value = 5
				OR = {
					area = khirnadhiman_area
					area = keyattordha_area
					area = khiraspid_area
					area = tughayasa_area
				}
			}
		}
		
		effect = {
			add_country_modifier = {
				name = ghavaanaj_dhujati_tusks
				duration = 7300
			}
		}
	}
	
	ghavaanaj_3_the_white_guild = {
		icon = mission_noble_council
		position = 7
		
		required_missions = {
			ghavaanaj_3_dhujati_tusks
		}
		
		provinces_to_highlight = {
			OR = {
				area = dhujat_region
			}
			NOT = {
				owned_by = ROOT
			}
		}
		
		trigger = {
			dhujat_region = {
				type = all
				owned_by = ROOT
			}
		}
		
		effect = {
			country_event = {
				id = ghavaanaj.1
			}
		}
	}
	
	ghavaanaj_3_export_monopolies = {
		icon = mission_noble_council
		position = 8
		
		required_missions = {
			ghavaanaj_3_the_white_guild
		}
		
		provinces_to_highlight = {
			OR = {
				superregion = rahen_superregion
				superregion = yanshen_superregion
				superregion = south_haless_superregion
			}
			trade_goods = ivory
			NOT = {
				owned_by = ROOT
			}
		}
		
		trigger = {
			custom_trigger_tooltip = {
				tooltip = ghavaanaj_3_export_monopolies_tt
				NOT = {
					rahen_superregion = {
						trade_goods = chinaware
						NOT = { owned_by = ROOT }
					}
					yanshen_superregion = {
						trade_goods = chinaware
						NOT = { owned_by = ROOT }
					}
					south_haless_superregion = {
						trade_goods = chinaware
						NOT = { owned_by = ROOT }
					}
				}
			}
			any_owned_province = {
				has_port = yes
				development = 20
				has_trade_building_trigger = yes
			}
		}
		
		effect = {
			random_owned_province = {
				limit = {
					has_port = yes
					development = 20
					has_trade_building_trigger = yes
				}
				random_list = {
					1 = { change_trade_goods = ivory }
					1 = { change_trade_goods = chinaware }
				}
				add_permanent_province_modifier = {
					name = ghavaanaj_white_guild_port
					duration = -1
				}
			}
		}
	}
	
	ghavaanaj_3_punish_atrocities = {
		icon = mission_noble_council
		position = 9
		
		required_missions = {
			ghavaanaj_3_export_monopolies
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			#! Control Prukakhin, have 30 units there
		}
		
		effect = {
			#! Prukakhin loses 10 dev and its CoT decreases by 1, trade good chances to fish. Capital gains 6 dev.
		}
	}
	
	ghavaanaj_3_a_different_kind_of_beast = {
		icon = mission_noble_council
		position = 10
		
		required_missions = {
			ghavaanaj_3_punish_atrocities
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			#! Have gone on 5 expeditions
		}
		
		effect = {
			#! Get the choice of shifting your training 2 steps or gaining a temporary bonus to training if at the max
		}
	}
	
	ghavaanaj_3_the_ivory_lotus = {
		icon = mission_noble_council
		position = 12
		
		required_missions = {
			ghavaanaj_2_the_great_trumpet
			ghavaanaj_4_a_time_of_blooming
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			OR = {
				NOT = { any_known_country = { has_reform = raja_reform } }
				has_reform = raja_reform
			}
			capital_scope = { development = 40 }
		}
		
		effect = {
			add_country_modifier = {
				name = ghavaanaj_the_ivory_lotus
				duration = -1
			}
		}
	}
}


ghavaanaj_4_1 = {
	slot = 4
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			culture = ghavaanaj
		}
	}
	
	ghavaanaj_4_the_black_tribe = {
		icon = mission_noble_council
		position = 2
		
		required_missions = {
			ghavaanaj_3_prepare_the_elephants
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4486
				province_id = 4487
				province_id = 4495
			}
			NOT = {
				AND = {
					owned_by = ROOT
					culture = ghavaanaj
					NOT = { nationalism = 1 }
				}
			}
		}
		
		trigger = {
			4486 = {
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
			4487 = {
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
			4495 = {
				owned_by = ROOT
				culture = ghavaanaj
				NOT = { nationalism = 1 }
			}
		}
		
		effect = {
			add_army_tradition = 10
		}
	}
}


ghavaanaj_4_2 = {
	slot = 4
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			tag = R72
		}
	}
	
	ghavaanaj_4_the_ivory_palace = {
		icon = mission_noble_council
		position = 4
		
		required_missions = {
			ghavaanaj_3_the_black_kings_city
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			capital_scope = { province_id = 4485 }
			adm_power = 100
			treasury = 250
		}
		
		effect = {
			add_adm_power = -100
			add_treasury = -250
			capital_scope = {
				add_permanent_province_modifier = {
					name = ghavaanaj_the_ivory_palace
					duration = -1
				}
			}
		}
	}
	
	ghavaanaj_4_the_city_of_mews = {
		icon = mission_noble_council
		position = 5
		
		required_missions = {
			ghavaanaj_4_the_ivory_palace
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_4_the_senapti_forts = {
		icon = mission_noble_council
		position = 6
		
		required_missions = {
			ghavaanaj_3_a_new_black_king
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_4_guardian_of_the_north = {
		icon = mission_noble_council
		position = 7
		
		required_missions = {
			ghavaanaj_4_the_senapti_forts
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_4_roles_reversed = {
		icon = mission_noble_council
		position = 8
		
		required_missions = {
			ghavaanaj_4_guardian_of_the_north
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_4_the_elephant_commands = {
		icon = mission_noble_council
		position = 9
		
		required_missions = {
			ghavaanaj_4_roles_reversed
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_4_shield_of_the_shamakhad = {
		icon = mission_noble_council
		position = 10
		
		required_missions = {
			ghavaanaj_4_the_elephant_commands
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_4_a_time_of_blooming = {
		icon = mission_noble_council
		position = 11
		
		required_missions = {
			ghavaanaj_4_shield_of_the_shamakhad
			ghavaanaj_5_the_peace_of_rahen
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
}


ghavaanaj_5_1 = {
	slot = 5
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			culture = ghavaanaj
		}
	}
	
	ghavaanaj_5_lord_and_champion = {
		icon = mission_noble_council
		position = 1
		
		required_missions = {
			
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_5_master_of_the_grounds = {
		icon = mission_noble_council
		position = 2
		
		required_missions = {
			ghavaanaj_5_lord_and_champion
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_5_expand_game_grounds = {
		icon = mission_noble_council
		position = 4
		
		required_missions = {
			ghavaanaj_3_the_black_kings_city
			ghavaanaj_5_master_of_the_grounds
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
}


ghavaanaj_5_2 = {
	slot = 5
	generic = no
	ai = yes
	has_country_shield = yes
	
	potential = {
		OR = {
			tag = R72
		}
	}
	
	ghavaanaj_5_the_greatest_games = {
		icon = mission_noble_council
		position = 5
		
		required_missions = {
			ghavaanaj_5_expand_game_grounds
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_5_the_indigo_guild = {
		icon = mission_noble_council
		position = 6
		
		required_missions = {
			
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_5_a_festival_of_all_colours = {
		icon = mission_noble_council
		position = 7
		
		required_missions = {
			ghavaanaj_5_the_indigo_guild
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_5_increase_indigo_production = {
		icon = mission_noble_council
		position = 8
		
		required_missions = {
			ghavaanaj_5_a_festival_of_all_colours
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
	
	ghavaanaj_5_the_peace_of_rahen = {
		icon = mission_noble_council
		position = 10
		
		required_missions = {
			ghavaanaj_5_increase_indigo_production
		}
		
		provinces_to_highlight = {
			
		}
		
		trigger = {
			
		}
		
		effect = {
			
		}
	}
}